package com.greatlearning.designpattern1;

import com.greatlearning.designpattern1.connections.DbConnection;

import java.sql.Connection;

public class JDBCSingletonDemo {
    public static void main(String[] args) {

        Connection dbConn = DbConnection.getConnection();

        System.out.println(" Now Demonstrating to check for Singleton Instance \n");

        System.out.println("Creating First Jdbc Connection Instance");
        Connection dbConn1 = DbConnection.getConnection();

        System.out.println("Creating Second Jdbc Connection Instance");
        Connection dbConn2 = DbConnection.getConnection();

        System.out.println("Creating Third Jdbc Connection Instance");
        Connection dbConn3 = DbConnection.getConnection();

        if (dbConn1 == dbConn2 && dbConn2 == dbConn3) {
            System.out.println("\n Thus, Singleton JDBC Object Creation is demonstrated");
        } else {
            System.out.println("\n Thus, Failed to create Singleton JDBC Object Creation");
        }
    }
}
