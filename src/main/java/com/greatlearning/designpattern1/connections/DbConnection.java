package com.greatlearning.designpattern1.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Creating a Single Ton JDBC Connection
 * Which is Thread safe and Lazy Initialization Method
 */

public class DbConnection {

    //Static Variable Declaration
    private static Connection conn;

    //This will be Called when only No Connection exists
    private static Connection getConnection(String dbUrl, Properties dbProp) {
        try {
            conn = DriverManager.getConnection(dbUrl, dbProp);
            System.out.println("Created a Global Reference JDBC Connection :" + conn);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Failed to establish JDBC Connection, Check log for More details");
            System.exit(1);
        }
        return conn;
    }

    // This will be the actual method that will be called
    public static Connection getConnection() {
        if (conn != null) {
            System.out.println(" Sharing Global JDBC Connection created with Object Reference As :" + conn);
            return conn;
        }

        // Only in case of No Instance Available , we crete an Instance
        String dbUrl = "jdbc:mysql://localhost:3306/userDb";
        Properties dbProp = new Properties();
        dbProp.put("user", "root");
        dbProp.put("password", "admin");
        return getConnection(dbUrl, dbProp);
    }
}
