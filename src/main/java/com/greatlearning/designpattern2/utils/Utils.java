package com.greatlearning.designpattern2.utils;

import java.util.Scanner;

public class Utils {
    public static int getNumber(){
        Scanner scanner= new Scanner(System.in);
        String inpVal;
        do {
            inpVal = scanner.next();
            if (!inpVal.matches("[0-9]+")) {
                System.out.println("Invalid number, Please re input ");
            }
        }while(!inpVal.matches("[0-9]+"));
        return Integer.parseInt(inpVal);
    }

}
