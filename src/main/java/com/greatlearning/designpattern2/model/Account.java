package com.greatlearning.designpattern2.model;

public class Account {

    private final int accountNo;
    private final String accountType;
    private final String branch;
    private final int accountBalance;

    //Optional Fields
    private String atmTransactions;
    private String emiSchedules;

    //Implementing Constructor with Only Mandatory fields
    public Account(int accountNo, String accountType, String branch, int accountBalance) {
        this.accountNo = accountNo;
        this.accountType = accountType;
        this.branch = branch;
        this.accountBalance = accountBalance;
    }

    //Implementing Constructor with All fields including optional
    public Account(int accountNo, String accountType, String branch, int accountBalance, String atmTransactions, String emiSchedules) {
        this.accountNo = accountNo;
        this.accountType = accountType;
        this.branch = branch;
        this.accountBalance = accountBalance;
        this.atmTransactions = atmTransactions;
        this.emiSchedules = emiSchedules;
    }

    //This Method will help in printing the object in User readable format
    @Override
    public String toString() {
        return "Account{" +
                "accountNo=" + accountNo +
                ", accountType='" + accountType + '\'' +
                ", branch='" + branch + '\'' +
                ", accountBalance=" + accountBalance +
                ", atmTransactions='" + atmTransactions + '\'' +
                ", emiSchedules='" + emiSchedules + '\'' +
                '}';
    }


}
