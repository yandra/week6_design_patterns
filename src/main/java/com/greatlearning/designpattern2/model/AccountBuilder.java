package com.greatlearning.designpattern2.model;

public class AccountBuilder {
    private int accountNo;
    private String accountType;
    private String branch;
    private int accountBalance;
    private String atmTransactions;
    private String emiSchedules;

    public AccountBuilder setAccountNo(int accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public AccountBuilder setAccountType(String accountType) {
        this.accountType = accountType;
        return this;
    }

    public AccountBuilder setBranch(String branch) {
        this.branch = branch;
        return this;
    }

    public AccountBuilder setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
        return this;
    }

    public AccountBuilder setAtmTransactions(String atmTransactions) {
        this.atmTransactions = atmTransactions;
        return this;
    }

    public AccountBuilder setEmiSchedules(String emiSchedules) {
        this.emiSchedules = emiSchedules;
        return this;
    }

    public Account createAccount() {
        return new Account(accountNo, accountType, branch, accountBalance,atmTransactions,emiSchedules);
    }
}
