package com.greatlearning.designpattern2;

import com.greatlearning.designpattern2.dao.Dao;
import com.greatlearning.designpattern2.dao.DaoImpl;
import com.greatlearning.designpattern2.model.Account;

import java.util.ArrayList;
import java.util.Scanner;

public class BuilderPatternDemo {
    public static void main(String[] args) {

        Dao dao = new DaoImpl();
        ArrayList<Account> accounts = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("!!!!! Welcome to Account Creation !!!!!" + "\n");

        //Adding some Default Accounts
        accounts.add(new Account(10001,"Savings","Jubli Hills",8500));
        accounts.add(new Account(10005,"Current","Whitefield",12000));
        accounts.add(new Account(10010,"Savings","HiTech City",7500));

        try {
            int choice;
            do {
                System.out.println("-----------------------------------");
                System.out.println("1. Account Creation");
                System.out.println("2. Display Accounts");
                System.out.println("0. Exit");
                System.out.println("-----------------------------------");
                choice = scanner.nextInt();

                switch (choice) {
                    case 1:
                        accounts.add(dao.getAccountDetails());
                        break;
                    case 2:
                        dao.displayAccountDetails(accounts);
                        break;
                    case 0:
                        System.out.println("Exiting Now");
                }
            } while (choice != 0);
        } catch (Exception e) {
            System.out.println("Something Failed , Please Try Again !!!");
            e.printStackTrace();
        }
    }
}
