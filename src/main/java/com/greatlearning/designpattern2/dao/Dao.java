package com.greatlearning.designpattern2.dao;

import com.greatlearning.designpattern2.model.Account;

import java.util.ArrayList;

public interface Dao {

    // by default access is public for interface methods
    Account getAccountDetails();
    void displayAccountDetails(ArrayList<Account> accounts);

}
