package com.greatlearning.designpattern2.dao;

import com.greatlearning.designpattern2.model.Account;
import com.greatlearning.designpattern2.model.AccountBuilder;
import com.greatlearning.designpattern2.utils.Utils;

import java.util.ArrayList;
import java.util.Scanner;

public class DaoImpl implements Dao{
    Scanner scanner =new Scanner(System.in);
    @Override
    public Account getAccountDetails(){
        System.out.println("Enter account No: ");
        int accountNo=Utils.getNumber();
        System.out.println("Enter account type (Savings/Current/etc): ");
        String accountType=scanner.next();
        System.out.println("Enter Branch Name: ");
        String branch=scanner.next();
        System.out.println("Enter Balance");
        int accountBalance=Utils.getNumber();

        // Building with Mandatory fields
        Account account = new AccountBuilder().setAccountNo(accountNo).setAccountType(accountType).setBranch(branch).setAccountBalance(accountBalance).createAccount();

        // Building with All fields Including Optional Fields (For Future)
        //Account account = new AccountBuilder().setAccountNo(accountNo).setAccountType(accountType).setBranch(branch).setAccountBalance(accountBalance).setEmiSchedules("Emi TXNs").setAtmTransactions("ATM Enabled").createAccount();

        System.out.println(" Congrats !!! Bank Account Created");
        return account;
    }

    public void displayAccountDetails(ArrayList<Account> accounts) {
        for (Account account : accounts)
            System.out.println(account);
    }
}
