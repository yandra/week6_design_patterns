package com.greatlearning.designpattern3.dao;

public abstract class CurrencyConverter {
    public abstract double getConvRate();
    public abstract double convertToINR(double amount);
}
