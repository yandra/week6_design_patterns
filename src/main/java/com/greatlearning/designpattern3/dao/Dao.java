package com.greatlearning.designpattern3.dao;

public interface Dao {

    CurrencyConverter currencyValidator(String currency);
}
