package com.greatlearning.designpattern3.dao;

public abstract class Currency {
    protected abstract String getCurrencyDescription();
}
