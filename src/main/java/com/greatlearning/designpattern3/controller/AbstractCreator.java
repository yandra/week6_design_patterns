package com.greatlearning.designpattern3.controller;

import com.greatlearning.designpattern3.dao.CurrencyConverter;
import com.greatlearning.designpattern3.daoimpl.DollarConverter;
import com.greatlearning.designpattern3.daoimpl.GBPConverter;
import com.greatlearning.designpattern3.daoimpl.GBPCurrency;
import com.greatlearning.designpattern3.daoimpl.USDCurrency;

public class AbstractCreator extends AbstractFactory{

    @Override
    public String getCurrencyDescription(String currency) {
        if (currency == null) {
            return null;
        }
        if(currency.equalsIgnoreCase("USD")){
            return new USDCurrency().getCurrencyDescription();
        } else if(currency.equalsIgnoreCase("GBP")){
            return new GBPCurrency().getCurrencyDescription();
        }
        return null;
    }

    @Override
    public CurrencyConverter getCurrencyConverter(String currency) {
        if (currency == null) {
            return null;
        }
        if(currency.equalsIgnoreCase("USD")){
            return new DollarConverter();
        } else if(currency.equalsIgnoreCase("GBP")){
            return new GBPConverter();
        }
        return null;
    }
}
