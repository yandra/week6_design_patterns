package com.greatlearning.designpattern3.controller;

import com.greatlearning.designpattern3.dao.CurrencyConverter;

abstract class AbstractFactory {
    public abstract String getCurrencyDescription(String currency);

    public abstract CurrencyConverter getCurrencyConverter(String currency);
}
