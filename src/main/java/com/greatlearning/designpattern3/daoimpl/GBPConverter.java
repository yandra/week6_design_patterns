package com.greatlearning.designpattern3.daoimpl;

import com.greatlearning.designpattern3.config.Config;
import com.greatlearning.designpattern3.dao.CurrencyConverter;

public class GBPConverter extends CurrencyConverter {

    @Override
    public double getConvRate() {
        return Config.GBP_INR_RATE;
    }

    @Override
    public double convertToINR(double amount) {
        return amount* getConvRate();
    }

}
