package com.greatlearning.designpattern3.daoimpl;

import com.greatlearning.designpattern3.controller.AbstractCreator;
import com.greatlearning.designpattern3.dao.CurrencyConverter;
import com.greatlearning.designpattern3.dao.Dao;

public class DaoImpl implements Dao {

    @Override
    public CurrencyConverter currencyValidator(String currencyName) {
        AbstractCreator currencyFactory = new AbstractCreator();
        String currencyDescription = currencyFactory.getCurrencyDescription(currencyName);
        if (currencyDescription != null) {
            System.out.println(" We are Happy to Convert your Currency " + currencyDescription);
            return currencyFactory.getCurrencyConverter(currencyName);
        } else {
            System.out.println(" we are currently Not Supporting Currency (" + currencyName + ") for Conversion to INR.\n  Please Come Back later !!! ");
            System.exit(0);
        }
        return null;
    }
}
