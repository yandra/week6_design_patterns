package com.greatlearning.designpattern3.daoimpl;

import com.greatlearning.designpattern3.dao.Currency;

public class GBPCurrency extends Currency {
    @Override
    public String getCurrencyDescription() {
        return "British pound sterling(GBP)";
    }
}
