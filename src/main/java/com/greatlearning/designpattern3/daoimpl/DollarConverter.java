package com.greatlearning.designpattern3.daoimpl;

import com.greatlearning.designpattern3.config.Config;
import com.greatlearning.designpattern3.dao.CurrencyConverter;

public class DollarConverter extends CurrencyConverter {

    @Override
    public double getConvRate() {
        return Config.USD_INR_RATE;
    }

    @Override
    public double convertToINR(double amount) {
        return amount* getConvRate();
    }
}
