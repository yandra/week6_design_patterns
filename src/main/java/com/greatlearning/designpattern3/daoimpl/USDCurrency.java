package com.greatlearning.designpattern3.daoimpl;

import com.greatlearning.designpattern3.dao.Currency;

public class USDCurrency extends Currency {

    @Override
    public String getCurrencyDescription() {
        return "United States dollar(USD)";
    }
}
