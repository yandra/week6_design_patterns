package com.greatlearning.designpattern3.utils;

import java.util.Scanner;

public class Utils {
    //Creating a Global instance of Scanner Object
    private static final Scanner scanner=new Scanner(System.in);

    public static double readNumber() {
        String inpVal;
        do {
            inpVal = scanner.next();
            if (!inpVal.matches("[0-9.]+")) {
                System.out.println("Invalid number, Please re input ");
            }
        } while (!inpVal.matches("[0-9.]+"));
        //return Integer.parseInt(inpVal);
        return Double.parseDouble(inpVal);
    }

    public static String readString() {
        String inpVal;
        inpVal = scanner.next();
        return inpVal;
    }
}
