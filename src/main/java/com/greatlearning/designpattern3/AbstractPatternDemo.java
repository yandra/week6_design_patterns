package com.greatlearning.designpattern3;

import com.greatlearning.designpattern3.dao.CurrencyConverter;
import com.greatlearning.designpattern3.dao.Dao;
import com.greatlearning.designpattern3.daoimpl.DaoImpl;
import com.greatlearning.designpattern3.utils.Utils;

public class AbstractPatternDemo {

    public static void main(String[] args) {
        double amount;
        Dao dao =new DaoImpl();
        CurrencyConverter converter;

        System.out.println("------Welcome to Currency Convertor------");

        System.out.println("\n Enter the Currency Name (Ex: USD/GBP/etc..) to Convert to INR ");
        String currencyName = Utils.readString();

        //Checking For Currency Existence And Conversion Eligibility
        converter = dao.currencyValidator(currencyName);

        System.out.println(" Enter Amount to Convert to INR :");
        amount = Utils.readNumber();

        //Performing the Conversion logic
        System.out.println("  Conversion Rate identified for Currency is :" + converter.getConvRate());
        System.out.println("  Final Converted value in INR : " + converter.convertToINR(amount));
    }
}
